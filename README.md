# virtualenv

## Problématique

Quand on programme sur beaucoup de projets différents, on commence par
accumuler du code qui peut avoir des pré-requis en conflit. Par exemple, un
projet peut nécessiter une version précise d'une lib alors que le développement
du produit se fera sur la dernière version releasée de la lib. De même, il est
bon de ne pas mélanger les libs du système avec celles spécifique au projet.

On a donc besoin d'*isoler* les projets entre eux, car sinon c'est un vrai
mélange dans le meilleur des cas, le chaos total de le pire :).

Ici on va s'intéresser à la solution pour python (puisqu'on fera nos exemples
en python). C'est un sujet générique et quasiment chaque langage / techno offre
des techniques pour résoudre cela. La question c'est de trouver celle qui nous
convient le mieux.

## Recherches

Mots clés pour nos recherches: *python project isolation*

-> https://towardsdatascience.com/virtual-environments-104c62d48c54

- venv
    - python3 -m venv
    - source venv/bin/activate

Cool ça marche ! Mais c'est pas super user friendly.

À la recherche d'un truc plus user friendly: *virtualenv tools*

-> https://towardsdatascience.com/comparing-python-virtual-environment-tools-9a6543643a44

- pipenv
    - inclus dans debian
    - La recherche de pipenv dans google:
        - le github est en pypa/pipenv (pypa est la python packaging
          authority)
            - 18251 étoiles
            - dernier commit récent
            - 290 contributeurs
        - lien vers la doc
            - Windows is a first class citizen
            - pipenv shell dans le répertoire du Pipfile pour activer le
              virtualenv
- pew
    - pas dans debian (un ITP vieux d'un an existe)
    - La recherche de pew dans google:
        - le github du projet
            - 1001 étoiles
            - dernier commit il y a quelques mois
            - 36 contributeurs
            - semble supporter windows
        - sametmax: http://sametmax.com/mieux-que-python-virtualenvwrapper-pew/
            - pew est mieux que virtualenvwrapper
        - reddit: https://www.reddit.com/r/Python/comments/3nj3ug/pew_python_env_wrapper_create_and_manage/
            - rien de bien neuf

- poetry
    - pas dans debian (pas d'ITP)
    - le github du projet
        - 6034 étoiles
        - dernier commit récent
        - 91 contributeurs
    - *poetry shell* ne fonctionne pas en dehors du virtualenv


## Résumé

### python3 -m venv

\+:
- intégré à python
- Test

\-:
- pas très user friendly

### pipenv

\+:
- Soutenu par la pypa
- Projet actif
- Windows est pris en compte

\-:
- Fait beaucoup d'autres choses
- nom du virtualenv qui est un hash (wtf!)
- Un nouveau fichier à gérer: *Pipfile* créé par l'initialisation du virtualenv
- *pipenv shell* crée un virtualenv quoiqu'il arrive.

### pew

\+:
- Supporte powershell et cygwin
- Nom configurable
- *pew mkproject*

\-:
- Plus petit projet

### virtualenvwrapper

Pas considéré

### poetry

\+:
- Projet actif
- meilleure résolution de dépendance que le pip de base
- nom du virtualenv ± configurable (s'il est créé avant or on cherche un outil
  pour gérer les virtualenv)
- supporte windows
- output sympa

\-:
- Fait beaucoup d'autres choses
- Un nouveau fichier à gérer: *pyproject.toml*
